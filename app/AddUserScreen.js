import React, { Component } from 'react';
import {
    Button,
    StyleSheet,
    ScrollView,
    ActivityIndicator,
    View,
    TouchableOpacity,
    Text,
} from 'react-native';
import firebase from '../app/firebaseDb';
import '@firebase/firestore';
import { ListItem } from 'react-native-elements';
import { TextInput, List } from 'react-native-paper';

const db = firebase.firestore();
db.settings({ experimentalForceLongPolling: true });

class AddUserScreen extends Component {
    constructor() {
        super();
        this.storeUser.bind(this);
        this.state = {
            name: '',
            email: '',
            mobile: '',
            isLoading: false,
            userArr: [],
            expanded: true,
        };
    }
    async componentDidMount() {
        this.unsubscribe = await db
            .collection('users')
            .onSnapshot(this.getCollection);
    }
    getCollection = (querySnapshot) => {
        const userArr = [];
        querySnapshot.forEach((res) => {
            const { name, email, mobile } = res.data();
            console.log(res.data());
            userArr.push({
                key: res.id,
                res,
                name,
                email,
                mobile,
            });
        });
        this.setState({
            userArr,
            isLoading: false,
        });
    };

    inputValueUpdate = (val, prop) => {
        const state = this.state;
        state[prop] = val;
        this.setState(state);
    };

    storeUser() {
        if (this.state.name === '') {
            alert('Fill at least your name!');
        } else {
            console.log('clic');
            console.log(this.state.name);

            db.collection('users')
                .add({
                    name: this.state.name,
                    email: this.state.email,
                    mobile: this.state.mobile,
                })
                .then((res) => {
                    this.setState({
                        name: '',
                        email: '',
                        mobile: '',
                        isLoading: false,
                    });
                    this.props.navigation.navigate('UserScreen');
                    this.unsubscribe();
                })
                .catch((err) => {
                    console.error('Error found: ', err);
                    this.setState({
                        isLoading: false,
                    });
                });
        }
    }
    _handlePress = () =>
        this.setState({
            expanded: !this.state.expanded,
        });
    render() {
        if (this.state.isLoading) {
            return (
                <View style={styles.preloader}>
                    <ActivityIndicator size="large" color="#9E9E9E" />
                </View>
            );
        }
        return (
            <ScrollView style={styles.container}>
                <View style={styles.inputGroup}>
                    <TextInput
                        mode={'outlined'}
                        label="Nombre"
                        value={this.state.name}
                        onChangeText={(val) => this.inputValueUpdate(val, 'name')}
                    />
                </View>
                <View style={styles.inputGroup}>
                    <TextInput
                        mode={'outlined'}
                        label="Email"
                        value={this.state.email}
                        onChangeText={(val) => this.inputValueUpdate(val, 'email')}
                    />
                </View>
                <View style={styles.inputGroup}>
                    <TextInput
                        mode={'outlined'}
                        label="Movil"
                        value={this.state.mobile}
                        onChangeText={(val) => this.inputValueUpdate(val, 'mobile')}
                    />
                </View>
                <View>
                    <TouchableOpacity
                        onPress={() => this.storeUser()}
                        style={{
                            backgroundColor: '#19AC52',
                            borderRadius: 10,
                            padding: 10,
                            marginBottom: 20,
                        }}>
                        <Text style={{ color: 'white', alignSelf: 'center' }}>
                            Agregar Usuarios
            </Text>
                    </TouchableOpacity>


                </View>
                <View>
                <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('UserScreen')}
                        style={{
                            backgroundColor: 'black',
                            borderRadius: 10,
                            padding: 10,
                            marginBottom: 20,
                        }}>
                        <Text style={{ color: 'white', alignSelf: 'center' }}>
                            Lista de Usuarios
            </Text>
                    </TouchableOpacity>
                </View>

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 35,
    },
    inputGroup: {
        flex: 1,
        padding: 0,
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#cccccc',
    },
    preloader: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default AddUserScreen;
